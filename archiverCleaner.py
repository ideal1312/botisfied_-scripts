import os
import praw
import datetime
import pymongo

session = praw.Reddit('bot')
client = pymongo.MongoClient()
db = client['archiveDB']
collection = db['data']

collection.remove({"created": {"$lt": datetime.datetime.utcnow() - datetime.timedelta(days=3)}})
