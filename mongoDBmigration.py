import pymongo
import praw
import os
from datetime import datetime

client = pymongo.MongoClient()
db = client['archiveDB']
collection = db['data']
session = praw.Reddit('bot')
fout = os.path.join(os.path.expanduser('~'), 'archivelinks')
if os.path.isfile(fout):
    with open(fout, "r") as f:
        lines = f.read().splitlines()
        for line in lines:
            x = line.split(" ")
            if collection.find_one({"id":x[0]}):
                continue
            submission = session.submission(id=x[0])
            post = {"id":x[0],"url":x[1],"created":datetime.utcfromtimestamp(submission.created_utc)}
            collection.insert_one(post)
