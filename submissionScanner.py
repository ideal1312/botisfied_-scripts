from archivenow import archivenow
import os
import praw
import requests
import time
import re
import pymongo
from datetime import datetime
from prawcore.exceptions import PrawcoreException

session = praw.Reddit('bot')
subreddit = session.subreddit('dachschaden')
client = pymongo.MongoClient()
db = client['archiveDB']
collection = db['data']

suizidText = "Wenn du denkst, du könntest suizidgefährdet sein, überleg bitte, die Telefon-Seelsorge unter der 0800-1110111 oder der 0800-1110222 anzurufen. Alternativ kannst du auch unter https://chat.telefonseelsorge.org/index.php chatten. Falls du Personen kennst, die womöglich suizidgefährdet sein könnten, mach sie bitte auf diese Angebote aufmerksam! Weitere Informationen findest du auf http://www.telefonseelsorge.de/.\n\nHier noch einige weitere Links zu relevanter Information:\n\n* [Was ist eine Depression?](http://www.neurologen-und-psychiater-im-netz.org/psychiatrie-psychosomatik-psychotherapie/erkrankungen/depressionen/was-ist-eine-depression/)\n* [Was ist Psychotherapie?](http://www.neurologen-und-psychiater-im-netz.org/psychiatrie-psychosomatik-psychotherapie/therapie/psychotherapie/was-ist-psychotherapie/)\n* [Persönliche Selbsthilfe](http://www.neurologen-und-psychiater-im-netz.org/selbsthilfe-angehoerige/selbsthilfe/persoenliche-selbsthilfe/)\n* [Was bezahlt die Krankenkasse?](https://www.therapie.de/psyche/info/fragen/wichtigste-fragen/was-bezahlt-die-krankenkasse/)\n\n^^Dieser ^^Kommentar ^^wird ^^automatisch ^^durch ^^CN/CW:Suizid ^^gepostet. ^^Vielen ^^Dank ^^an ^^/r/einfach_posten ^^für ^^das ^^Bereitstellen ^^des ^^Materials"
running = True
while running:
    try:
        for submission in subreddit.stream.submissions(skip_existing=True):
            if submission.author.name == 'botisfied_' or hasattr(submission, 'crosspost_parent_list'):
                continue
            if re.search("C[NW]:? ?Suizid", submission.selftext + submission.title, re.IGNORECASE):
                sticky = submission.reply(suizidText)
                sticky.disable_inbox_replies()
                if not submission.comments or (submission.comments and not submission.comments[0].stickied):
                    sticky.mod.distinguish(how='yes',sticky=True)
            if not submission.is_self and "text/html" in requests.head(submission.url).headers.get("content-type",""):        
                try:
                    collection.insert_one({"id":str(submission.id),"url":str(archivenow.push(submission.url,"ia")[0]),"created":datetime.utcnow()})
                except Exception as ex:
                    print(ex)
    except KeyboardInterrupt:
        print("termination received. Goodbye!")
        running = False
    except PrawcoreException:
        print("run loop")
        time.sleep(10)
