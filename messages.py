#!/usr/bin/python
import praw
import os
import time

from prawcore.exceptions import PrawcoreException

r = praw.Reddit('bot')
running = True
while running:
    try:
        for message in r.inbox.stream():
            if message.parent_id:
                try:
                    flaired = False
                    parentMessage = r.inbox.message(message.parent_id.replace('t4_',''))
                    post = r.submission(id=parentMessage.body.split('\n')[0].split('/')[3])
                    for ch in post.flair.choices():
                        if ch['flair_text'] in message.body:
                            post.flair.select(ch['flair_template_id'])
                            message.reply("Flair gesetzt: **{}**".format(ch['flair_text']))
                            flaired = True
                            break
                    if not flaired:
                        message.reply("Keinen passenden Flair in **{}** gefunden".format(message.body))
                except:
                    print("can't do: " + message.body)
            else:
                lines = message.body.splitlines()
                for line in lines:
                    x = line.split(':')
                    if x[0]=='flairStats':
                        path = os.path.join(os.path.expanduser('~'),'statlogs', x[1])
                        if os.path.isfile(path):
                            with open(path, 'r') as f:
                                message.reply(f.read().replace('\n','\n\n'))
                        else:
                            message.reply('Statistik fuer den Monat' + x[1] + ' konnte nicht gefunden werden')
            message.mark_read()
    except KeyboardInterrupt:
        print("Termination received. Goodbye!")
        running = False
    except PrawcoreException:
        print('run loop')
        time.sleep(10)
