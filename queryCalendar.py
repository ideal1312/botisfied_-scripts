from __future__ import print_function
import datetime
from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
import os
# If modifying these scopes, delete the file token.json.
SCOPES = 'https://www.googleapis.com/auth/calendar.readonly'

def main():
    """Shows basic usage of the Google Calendar API.
    Prints the start and name of the next 10 events on the user's calendar.
    """
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    
    dir_path = os.path.dirname(os.path.abspath(__file__))
    store = file.Storage(dir_path + '/token.json')
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets(dir_path + '/credentials.json', SCOPES)
        creds = tools.run_flow(flow, store)
    service = build('calendar', 'v3', http=creds.authorize(Http()))

    # Call the Calendar API
    today = datetime.date.today()
    now= today + datetime.timedelta(days=-today.weekday())
    print(str(now))
    #now = datetime.datetime.now()
    then = now + datetime.timedelta(days=7, hours=12)
    print(str(then))
    #print('Getting the events for the upcoming week')
    print('getting the events from ' + now.isoformat() + ' to ' + then.isoformat())
    events_result = service.events().list(calendarId='primary', timeMin=now.isoformat() + 'T00:00:00Z',
            timeMax=then.isoformat() + 'T00:00:00Z', singleEvents=True,
                                        orderBy='startTime').execute()
    events = events_result.get('items', [])

    if not events:
        print('No upcoming events found.')
    else:
        return events

if __name__ == '__main__':
    main()
