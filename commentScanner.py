import os
import praw
import pymongo
import requests
import time
import re

from prawcore.exceptions import PrawcoreException

session = praw.Reddit('bot')
subreddit = session.subreddit('dachschaden')
client = pymongo.MongoClient()
db = client['archiveDB']
collection = db['data']

suizidText = "Wenn du denkst, du könntest suizidgefährdet sein, überleg bitte, die Telefon-Seelsorge unter der 0800-1110111 oder der 0800-1110222 anzurufen. Alternativ kannst du auch unter https://chat.telefonseelsorge.org/index.php chatten. Falls du Personen kennst die womöglich suizidgefährdet sein könnten, mach sie bitte auf diese Angebote aufmerksam! Weitere Informationen findest du auf http://www.telefonseelsorge.de/.\n\nHier noch einige weitere Links zu relevanter Information:\n\n* [Was ist eine Depression?](http://www.neurologen-und-psychiater-im-netz.org/psychiatrie-psychosomatik-psychotherapie/erkrankungen/depressionen/was-ist-eine-depression/)\n* [Was ist Psychotherapie?](http://www.neurologen-und-psychiater-im-netz.org/psychiatrie-psychosomatik-psychotherapie/therapie/psychotherapie/was-ist-psychotherapie/)\n* [Persönliche Selbsthilfe](http://www.neurologen-und-psychiater-im-netz.org/selbsthilfe-angehoerige/selbsthilfe/persoenliche-selbsthilfe/)\n* [Was bezahlt die Krankenkasse?](https://www.therapie.de/psyche/info/fragen/wichtigste-fragen/was-bezahlt-die-krankenkasse/)\n\n^^Dieser ^^Kommentar ^^wird ^^automatisch ^^durch ^^CN/CW:Suizid ^^gepostet. ^^Vielen ^^Dank ^^an ^^/r/einfach_posten ^^für ^^das ^^Bereitstellen ^^des ^^Materials"

running = True
while running:
    try:
        for comment in subreddit.stream.comments(skip_existing=True):
            if comment.author.name == 'botisfied_':
                continue
            text = ""
            if "#ABO" in comment.body and "text/html" in requests.head(comment.submission.url).headers.get("content-type", ""):
                post = collection.find_one({"id":comment.submission.id})               
                if post:
                    text = "Aufgrund der aktivierten Paywall: [Artikel auf archive.org](" + post["url"] + ")"
                    collection.delete_one({"id":comment.submission.id})
            if re.search("C[NW]:? ?Suizid",comment.body, re.IGNORECASE):
                text = suizidText
            if text:
                prevComment = ""
                for comment in comment.submission.comments:
                    if comment.author.name == 'botisfied_':
                        prevComment = comment
                        break
                if prevComment:
                    if not text in prevComment.body:
                        prevComment.edit(comment.body + '\n\n' +text)
                        if comment.submission.comments and not prevComment.submission.comments[0].stickied:
                            prevComment.mod.distinguish(how='yes',sticky=True)
                    continue
                sticky = comment.submission.reply(text)
                sticky.disable_inbox_replies()
                if not comment.submission.comments or (comment.submission.comments and not comment.submission.comments[0].stickied):
                    sticky.mod.distinguish(how='yes',sticky=True)
    except KeyboardInterrupt:
        print("termination received. Goodbye!")
        running = False;
    except PrawcoreException:
        print("run loop")
        time.sleep(10)
