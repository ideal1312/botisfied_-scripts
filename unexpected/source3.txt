﻿"https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Norbert_Barlicki_foto_%28cropped%29.jpg/800px-Norbert_Barlicki_foto_%28cropped%29.jpg","Norbert Barlicki Born 06/06/1880 - Murdered","Lawyer, Publicist, Politician"

"https://upload.wikimedia.org/wikipedia/commons/5/5c/Rosette_Wolczak.jpg","Rosette Wolczak - Born 04/19/1928 - Murdered","Rosette 'Rose' Wolczak was a Jewish child victim of the Holocaust. Born in France in 1928, she came to Geneva, Switzerland, in 1943 as a refugee, and was expelled for what the Swiss authorities ruled to be indecent behavior. She was sent to the Auschwitz concentration camp, where she was gassed upon her arrival in November 1943."

"https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Bronis%C5%82aw_Czech_1934_%28cropped%29.jpg/220px-Bronis%C5%82aw_Czech_1934_%28cropped%29.jpg","Bronisław Czech - Born 07/25/1908 - Murdered","Bronisław 'Bronek' Czech was a Polish sportsman and artist. A gifted skier, he won championships of Poland 24 times in various skiing disciplines, including Alpine skiing, Nordic skiing and ski jumping. A member of the Polish national team at three consecutive Winter Olympics, he was also one of the pioneers of mountain rescue in the Tatra Mountains and a glider instructor. He perished in the Auschwitz concentration camp."

"https://upload.wikimedia.org/wikipedia/en/thumb/f/f1/Lea_Deutsch.jpg/150px-Lea_Deutsch.jpg","Lea Deutsch - Born 04/18/1927- Murdered","Child actress"

"https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Fondane_in_his_youth.jpg/250px-Fondane_in_his_youth.jpg","Benjamin Fondane - Born 11/14/1898 - Murdered","Poet, Critic, Philosoph"

"https://upload.wikimedia.org/wikipedia/en/thumb/9/90/Miroslav_%C5%A0alom_Freiberger.jpg/220px-Miroslav_%C5%A0alom_Freiberger.jpg","Miroslav Šalom Freiberger - Born 01/09/1903- Murdered","Rabbi"

"https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Bundesarchiv_Bild_102-11401%2C_Berlin%2C_Filmschauspieler_bei_Kochkunstausstellung.jpg/220px-Bundesarchiv_Bild_102-11401%2C_Berlin%2C_Filmschauspieler_bei_Kochkunstausstellung.jpg","Kurt Gerron- Born 05/11/1897 - Murdered","Actor and director (Gerron is the man on the right)"

"https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Ala_Gertner_%281912-1945%29.jpg/220px-Ala_Gertner_%281912-1945%29.jpg","Ala Gertner - Born 04/12/1912- Murdered","Hanged in Auschwitz for beeing part of Sonderkommando revolt"

"https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Roza_Robota_%281921-1945%29.jpg/220px-Roza_Robota_%281921-1945%29.jpg","Roza Robota - Born 1921 - Murdered","Hanged in Auschwitz for beeing part of Sonderkommando revolt"

"https://upload.wikimedia.org/wikipedia/commons/1/17/PavelHaas.jpg","Pavel Haas - Born 06/21/1899 - Murdered","Composer"

"https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/Jane_Haining_Portrait.jpg/220px-Jane_Haining_Portrait.jpg","Jane Haining - Born 06/06/1897 - Murdered","Missionary"

"https://upload.wikimedia.org/wikipedia/en/thumb/6/66/Ivana_Hirschmann.jpg/150px-Ivana_Hirschmann.jpg","Ivana Hirschmann - Born 05/05/1866 - Murdered","Gymnastics professor"

"https://upload.wikimedia.org/wikipedia/commons/e/e8/Hans_Kr%C3%A1sa_%281899-1944%29.jpg","Hans Krása - Born 11/30/1899 - Murdered","Composer"

"https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Viktor_Ullmann_2.jpg/220px-Viktor_Ullmann_2.jpg","Viktor Ullmann - Born 01/01/1898- Murdered","Composer and pianist"

"https://upload.wikimedia.org/wikipedia/en/thumb/4/49/Rafael_Schachter.jpg/200px-Rafael_Schachter.jpg","Rafael Schächter - Born 05/25/1905 - Murdered","Composer and pianist"

"https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/Etty_Hillesum_1939.jpg/220px-Etty_Hillesum_1939.jpg","Etty Hillesum - Born 01/15/1914 - Murdered","Writer"

"https://upload.wikimedia.org/wikipedia/en/thumb/4/41/ReginaJonas.jpg/220px-ReginaJonas.jpg","Regina Jonas - Born 08/03/1902 - Murdered","Rabbi"

"https://upload.wikimedia.org/wikipedia/commons/thumb/e/ea/Itzhak_Katzenelson.jpg/220px-Itzhak_Katzenelson.jpg","Itzhak Katzenelson - Born 07/01/1886 - Murdered","Teacher, Poet, Dramatist"

"https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Fr.Maximilian_Kolbe_1939.jpg/220px-Fr.Maximilian_Kolbe_1939.jpg","Maximilian Kolbe - Born 01/08/1894 - Murdered","Volunteered to die in place of a stranger
Canonized in 1982"

"https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Rutka_Laskier.jpg/220px-Rutka_Laskier.jpg","Rutka Laskier - Born 06/12/1929 - Murdered","Rut 'Rutka' Laskier was a young Jewish diarist from Poland who is best known for her 1943 diary chronicling the three months of her life during the Holocaust. She was murdered at Auschwitz concentration camp in 1943 at the age of fourteen. Her manuscript, authenticated by Holocaust scholars and survivors, was published in the Polish language for the first time ever in early 2006. It has been compared to the diary of Anne Frank."

"https://upload.wikimedia.org/wikipedia/commons/thumb/0/0f/Rudolf_Levy.jpg/215px-Rudolf_Levy.jpg","Rudolf Levy - Born 07/15/1875- Murdered","Painter"

"https://upload.wikimedia.org/wikipedia/en/thumb/8/88/Natan%2C_Bernard%2C_French_movieproducer_on_trial_1936_circa.JPG/220px-Natan%2C_Bernard%2C_French_movieproducer_on_trial_1936_circa.JPG","Bernard Natan - Born 07/14/1886- Murdered","Director"

"https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Irene_Nemirovsky_25yo.jpg/200px-Irene_Nemirovsky_25yo.jpg","Irène Némirovsky - Born 02/24/1903 - Murdered","Novelist"

"https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/PIC_1-M-1222-4_Mi%C4%99dzynarodowe_zawody_lekkoatletyczne_na_stadionie_White_City_w_Londynie_1936.jpg/240px-PIC_1-M-1222-4_Mi%C4%99dzynarodowe_zawody_lekkoatletyczne_na_stadionie_White_City_w_Londynie_1936.jpg","Józef Noji - Born 09/08/1909- Murdered","Long-distance runner"

"https://upload.wikimedia.org/wikipedia/en/thumb/2/28/Self_Portrait_with_Jewish_Identity_Card_-Felix_Nussbaum_-_1943.jpg/220px-Self_Portrait_with_Jewish_Identity_Card_-Felix_Nussbaum_-_1943.jpg","Felix Nussbaum - Born 12/11/1904- Murdered","Painter"

"https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/Pasimagi_Selfportrait.jpg/185px-Pasimagi_Selfportrait.jpg","Karl Pärsimägi - Born 05/11/1902 - Murdered","Painter"

"https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Grzegorz_Peradze.png/220px-Grzegorz_Peradze.png","Grigol Peradze - Born 09/13/1899 - Murdered","Theologian and Professor, Canonized in 1995"

"https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Roman_Rybarski.jpg/200px-Roman_Rybarski.jpg"," - Born 07/03/1887 - Murdered","Economist"

"https://en.wikipedia.org/wiki/File:OttoErichSalomon1935.jpg","Erich Salomon - Born 04/28/1886 - Murdered","News Photographer (Salomon is the person on the right)"

"https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Malva_Schalek_-_Autoportrait.jpg/220px-Malva_Schalek_-_Autoportrait.jpg","Malva Schalek - Born 02/18/1882- Murdered","Painter"

"https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Else_berg_-_portrait_of_Mommie_Schwarz_1936.jpg/220px-Else_berg_-_portrait_of_Mommie_Schwarz_1936.jpg","Mommie Schwarz - Born 07/28/1876- Murdered","Painter"

"https://upload.wikimedia.org/wikipedia/en/thumb/b/b3/Selz.gif/180px-Selz.gif","Otto Selz - Born 02/14/1881- Murdered","Psychologist"

"https://upload.wikimedia.org/wikipedia/en/thumb/7/7a/Industrialist_Lavoslav_Singer.jpg/200px-Industrialist_Lavoslav_Singer.jpg","Lavoslav Singer - Born 1866 - Murdered","Industrialist"

"https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Edith_Stein_%28ca._1938-1939%29.jpg/220px-Edith_Stein_%28ca._1938-1939%29.jpg","Edith Stein - Born 10/12/1891 - Murdered","Nun, Philosopher
Canonized 1998"

"https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Chaim_Rumkowski_Hans_Biebow.jpg/220px-Chaim_Rumkowski_Hans_Biebow.jpg","Chaim Rumkowski - Born 02/27/1877 - Murdered","Rumkowski is the person on the left, photographed in the Łódź Ghetto"

"https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Sketch_of_Carlo_Taube.gif/200px-Sketch_of_Carlo_Taube.gif","Carlo Taube - Born 07/04/1897 - Murdered","Pianist"

"https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Weisz_%C3%81rp%C3%A1d.jpg/220px-Weisz_%C3%81rp%C3%A1d.jpg","Árpád Weisz - Born 01/31/1886 - Murdered","Football player"

"https://upload.wikimedia.org/wikipedia/en/thumb/b/b8/Mala_zimetbaum.jpg/260px-Mala_zimetbaum.jpg","Mala Zimetbaum - Born 01/26/1918 - Murdered","Known for the resistance she displayed at her execution"

"https://upload.wikimedia.org/wikipedia/en/thumb/b/bc/Galinski.jpg/200px-Galinski.jpg","Edward Galiński - Born 05/10/1923 - Murdered","A Pole, Edward 'Edek' Galiński, who was in love with Zimetbaum, planned to escape from the camp with his friend Wieslaw Kielar (Auschwitz survivor and author of autobiographical book Anus Mundi: 5 Years in Auschwitz). The plan fell through when Kielar lost a pair of SS guard's uniform pants needed as a disguise for their escape. Galiński told his friend that he would escape with Zimetbaum instead and would later find a way to send the uniform back to Kielar for his subsequent escape."

"https://upload.wikimedia.org/wikipedia/en/thumb/4/47/Eddy_Hamel_photograph.jpg/130px-Eddy_Hamel_photograph.jpg","Eddy Hamel - Born 10/21/1902 - Murdered","Soccer player"

"https://upload.wikimedia.org/wikipedia/commons/5/56/Rosa_Stallbaumer%2C_1897-1942%2C_head_and_shoulders_view.jpg","Rosa Stallbaumer - Born 11/30/1897 - Murdered","Rosa (Hoffman) Stallbaumer was a member of the Austrian Resistance during World War II. Her name is one of 124 names of women and men from Tyrol, Austria inscribed on the Liberation Monument at The Eduard-Wallnöfer-Platz in Innsbruck in recognition of both her involvement in resisting National Socialism and of her death at Auschwitz, following her incarceration at that Nazi concentration camp as punishment for helping Jewish targets of Nazi persecution escape to Italy."
