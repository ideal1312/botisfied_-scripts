#!/usr/bin/python3
import praw
from time import time

time_until_message = 180
add_flair_subject_line = "Da fehlt noch ein Flair"
add_flair_message = ("{post_url} \n\n"
                     "Eine Auswahl verfügbarer Flairs findest du [hier](https://old.reddit.com/r/Dachschaden/wiki/flairs)\n\n"
                     "Du kannst auf diese Nachricht auch mit dem gewünschten Flair antworten, ich mach dann den Rest :)")
session = praw.Reddit('bot')
subreddit = session.subreddit('dachschaden')
for submission in subreddit.search('subreddit:dachschaden',sort='new',time_filter='hour'):
    if submission.link_flair_text is None and (time() - submission.created_utc) > time_until_message:
        final_add_flair_message = add_flair_message.format(post_url=submission.shortlink)
        print("Sent Message to : {}".format(submission.author))
        session.redditor(submission.author.name).message(add_flair_subject_line, final_add_flair_message)
