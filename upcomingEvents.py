import feedparser
import praw
import datetime
from dateutil import parser
from bs4 import BeautifulSoup
import re
import urlmarker

eventStrings = []
feedurl = "http://termine.161.social/feed/rss"

feed = feedparser.parse(feedurl)
today = datetime.date.today()
then = today + datetime.timedelta(days= 14, hours=12)
for event in feed.entries:
    datestring=event.title[1:9]
    date = datetime.datetime.strptime(datestring,'%y-%m-%d').date()
    if today <= date <= then:
        infoList = []
        infoList.append('###[' + event.title + '](' + event.link + ')')
        eventString = '  \n'.join(infoList)
        eventStrings.append(eventString)
submitString = '\n___\n'.join(eventStrings)
r = praw.Reddit('bot')
sub = r.subreddit('dachschaden')
title = 'Anstehende Demonstrationen'
result = next(iter(list(sub.search(title,sort='new',time_filter='year'))),None)
if result and result.author == 'botisfied_' and result.archived == False:
   result.edit(submitString)
else:    
   post = sub.submit(title,selftext=submitString,send_replies=False, discussion_type='CHAT')
   for ch in post.flair.choices():
       if ch['flair_text'] == 'Aktivismus':
           post.flair.select(ch['flair_template_id'])
           break
