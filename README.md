# botisfied_ scripts

flairCheck.py - checks the submissions of the last hour for unflaired posts, messages the author about the misssing flair. Has to be run exactly once per hour

flairStats.py - checks all posts submitted within the last 24 hours and logs the number of occurences of every flair in a file. the file changes once a month. Has to be run exactly once per day

messages.py - checks the inbox of the bot to react to messages. Currently can set flairs in response to flairCheck.py and reply the flairStats.py logfile via "flairStats:YYYYMM"

queryCalendar.py - querys upcoming events (7 days) from the google calendar

upcomingEvents.py - formats the events from queryEvents.py and posts them on the subreddit