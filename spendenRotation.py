import praw
import re
from datetime import datetime, timedelta
session = praw.Reddit('bot')

orgas = [['Seebrücke', 'seebruecke'],
        ['Dorf der Jugend', 'Grimma'],
        ['Sea-Watch','sea-watch'],
        ['Rote Hilfe', 'rote-hilfe'],
        ['Rojava','rojava'],
        ['riseup', 'rise-up'],
        ['Reporter ohne Grenzen', 'reporter-ohne-grenzen'],
        ['Amadeu-Antonio Stiftung', 'Amadeu-Antonio-Stiftung'],
        ['Viva Con Agua', 'viva-con-agua'],
        ['Apabiz', 'Apabiz'],
        ['Ende Gelände', 'endeGelaende']]
        
kw = int((datetime.now() + timedelta(days=7)).strftime('%W'))
weeklyOrga = orgas[kw % (len(orgas)-1)]
subreddit = session.subreddit('dachschaden')
wikipage = subreddit.wiki[weeklyOrga[1]]
url = 'https://reddit.com/r/dachschaden/wiki/' + weeklyOrga[1]
text = 'Diese Woche: ' + weeklyOrga[0] + '\n\n' + '[weitere Infos](' + url + ')\n\n' + wikipage.content_md.split('Spende jetzt:',1)[1]
text = text.replace('####', '######')
desc = re.sub('Diese Woche:(?s)(.*?)&nbsp;', text + '\n &nbsp;', subreddit.description)
subreddit.mod.update(description = desc)
for widget in subreddit.widgets.sidebar :
    if isinstance(widget, praw.models.TextArea) and widget.text.startswith('Diese Woche:'):
        widget.mod.update(text=text)
