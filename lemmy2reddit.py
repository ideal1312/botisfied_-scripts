import praw
import feedparser
import datetime
import pytz
import re

session = praw.Reddit('bot')
subreddit = session.subreddit('dachschaden')
feedurl = 'https://lemmy.161.social/feeds/local.xml?sort=New'
feed = feedparser.parse(feedurl)
now = datetime.datetime.now(pytz.utc)
for post in feed.entries:
    datestring = post.published
    published = datetime.datetime.strptime(datestring, '%a, %d %b %Y %H:%M:%S %z')
    if now - datetime.timedelta(hours = 1) <= published and not '/c/spenden' in post.description and not '/c/glossar' in post.description and not '/c/meta' in post.description:
        link = post.comments
        title = post.title
        submission = subreddit.submit(title,url=link,send_replies=False)
        text = """Dieser Beitrag wurde auf https://lemmy.161.social/ gepostet. Lemmy ist eine dezentrale Redditalternative, auf die wir langsam umsteigen wollen. Um das nicht zu verpassen solltest du dir einen Account anlegen.
        
Um nicht auf zwei Plattformen zu diskutieren, ist dieser Post locked. Du kommst über den Link oben direkt in die Kommentare auf Lemmy."""
        comment = submission.reply(text)
        comment.disable_inbox_replies()
        comment.mod.distinguish(how='yes',sticky=True)
        submission.mod.lock()
