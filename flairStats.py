#!/usr/bin/python3
import praw
import os
import datetime

r = praw.Reddit('bot')
flairDict = {}
for template in r.subreddit('dachschaden').flair.link_templates:
    result = list(r.subreddit('dachschaden').search('flair_name:"' + template['text']+ '"',sort='new',time_filter='day'))
    flairDict[template['text']]=len(result)

fout = os.path.join(os.path.expanduser('~'), 'statlogs', datetime.datetime.now().strftime('%Y%m'))
#merge existing data with new data
if os.path.isfile(fout):
    with open(fout, "r") as f:
        lines = f.read().splitlines()
        for line in lines:
            x = line.split(">>>")
            flairDict[x[0]] = flairDict[x[0]] + int(x[1])
#write merged data to file
with open(fout, "w") as f: 
    for k,v in flairDict.items():
        f.write(str(k) + '>>>' + str(v) + '\n')
