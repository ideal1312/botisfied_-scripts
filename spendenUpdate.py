import praw
import re
from datetime import datetime, timedelta
import urllib.request
import json
session = praw.Reddit('bot')
orgas = ['sea-watch','rote-hilfe','rojava','rise-up','reporter-ohne-grenzen','Amadeu-Antonio-Stiftung','Viva-con-Agua','Apabiz','seebruecke', 'Grimma']
        
subreddit = session.subreddit('dachschaden')
for orga in orgas:
    wikipage = subreddit.wiki[orga]
    if 'betterplace.org' not in wikipage.content_md:
        continue
    link = re.findall('https:\/\/www\.betterplace\.org[^)]*',wikipage.content_md)[0]
    eventid = re.findall('\d+', link)[0] 
    api = 'https://www.betterplace.org/de/api_v4/fundraising_events/' + eventid
    amount = 0
    with urllib.request.urlopen(api) as url:
        data = json.loads(url.read().decode())
        amount = "%0.2f" % (data['donated_amount_in_cents']/100)
    updatedContent = re.sub('Bisheriger Betrag: \*\*\d+\.*\d+€\*\*', 'Bisheriger Betrag: **' + amount + '€**',wikipage.content_md)
    wikipage.edit(updatedContent)
